#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#define PORT 8080
 
bool checkClosed(int valread, int sock) {
    if(valread == 0) {
        close(sock);
        return 1;
    }
    return 0;
}
 
int main(int argc, char const *argv[]) {
    bool mode_input = false;
 
    if(getuid()) {
        if(argc < 5) {
            printf("too few argument passed\n");
            return -1;
        }
        if(strcmp(argv[1],"-u") || strcmp(argv[3],"-p")) {
            printf("syntax is not appropriate\n");
            return -1;
        }
        if(argc == 7) {
            if(!strcmp(argv[5],"-d")) {
                mode_input = true;
            }
            else {
                printf("syntax is not appropriate\n");
                return -1;
            }
        }
    }
    else {
        if(argc == 3) {
            if(!strcmp(argv[1],"-d")) {
                mode_input = true;
            }
            else {
                printf("syntax is not appropriate\n");
                return -1;
            }
        }
    }
 
    struct sockaddr_in address;
    int sock = 0, valread;
    struct sockaddr_in serv_addr;
 
    if((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }
 
    memset(&serv_addr, '0', sizeof(serv_addr));
 
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
     
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
 
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }
 
    char type[1024];
 
    if(getuid()) {
        strcpy(type, argv[2]);
        strcat(type, " ");
        strcat(type, argv[4]);
        send(sock, type, strlen(type), 0);
    }
    else{
        strcpy(type, "root");
        send(sock, type, strlen(type), 0);
    }
 
    char login_stat[1000] = {0};
    int val = recv(sock, login_stat, 1000, 0);
 
    if(!strcmp(login_stat, "authentication failed")) {
        close(sock);
        return -1;
    }
 
    if(getuid()) {
        if(mode_input) {
            char cmd[1000] = {0}, received[1000] = {0};
            sprintf(cmd, "USE %s", argv[6]);
            send(sock, cmd, strlen(cmd), 0);
            int val = recv(sock, received, 1000, 0);
 
            if(!strncmp(received, "database changed to", 19)) {
                char tmp[1000];
 
                while((fscanf(stdin,"%[^\n]%*c",tmp)) != EOF) {
                    send(sock, tmp, strlen(tmp), 0);
                    bzero(received, sizeof(received));
                    int val = recv(sock, received, 1000, 0);
                };
                close(sock);
                return 0;
            }
        }
    }
    else {
        if(mode_input) {
            char cmd[1000] = {0}, received[1000] = {0};
            sprintf(cmd, "USE %s", argv[2]);
            send(sock, cmd, strlen(cmd), 0);
            int val = recv(sock, received, 1000, 0);
 
            if(!strncmp(received, "database changed to", 19)){
                char tmp[1000];
 
                while((fscanf(stdin,"%[^\n]%*c",tmp)) != EOF) {
                    send(sock, tmp, strlen(tmp), 0);
                    bzero(received, sizeof(received));
                    int val = recv(sock, received, 1000, 0);
                };
                close(sock);
                return 0;
            }
        }
    }
 
    while(1){
        char msg[1000] ={0}, buffer[1024] = {0};
        gets(msg);
        send(sock, msg, strlen(msg), 0);
        valread = recv(sock, buffer, 1024, 0);
 
        if(checkClosed(valread, sock)) {
            break;
        }
 
        if(!strcmp(buffer, "mulai")) {
            char cmd[1000] = {0}, received[1000] = {0};
            strcpy(cmd, "ok");
            send(sock, cmd, strlen(cmd), 0);
 
            do {
                bzero(received, sizeof(received));
                int val = recv(sock, received, 1000, 0);
 
                if(strcmp(received, "DONE!!!")) {                    
                    bzero(cmd, sizeof(cmd));
                    strcpy(cmd, "ok");
                    send(sock, cmd, strlen(cmd), 0);
                }
            } while(strcmp(received, "DONE!!!"));
 
            bzero(cmd, sizeof(cmd));
            strcpy(cmd, "ok");
            send(sock, cmd, strlen(cmd), 0);
            int val = recv(sock, buffer, 1024, 0);
        }
    }
    return 0;
}
